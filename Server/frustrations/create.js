'use strict';

const AWS = require('aws-sdk'); // eslint-disable-line import/no-extraneous-dependencies

const dynamoDb = new AWS.DynamoDB.DocumentClient();

module.exports.create = (event, context, callback) => {
  const timestamp = new Date().getTime();
  const data = JSON.parse(event.body);
  const moronId = data.moronId;
  delete data.moronId;
  data.createdAt = timestamp;

  const params = {
    TableName: process.env.DYNAMODB_TABLE,
    Key: {
      id: moronId,
    },
    ExpressionAttributeNames: {
      '#frustrations': 'frustrations',
    },
    ExpressionAttributeValues: {
        ':frustration': [data],
        ':empty_list': [],
    },
    UpdateExpression: 'set #frustrations = list_append(if_not_exists(#frustrations, :empty_list), :frustration)',
    ReturnValues: 'ALL_NEW',
  };

  // update the todo in the database
  dynamoDb.update(params, (error, result) => {
    // handle potential errors
    if (error) {
      console.error(error);
      callback(null, {
        statusCode: error.statusCode || 501,
        headers: { 'Content-Type': 'text/plain' },
        body: 'Couldn\'t add the frustration',
      });
      return;
    }

    // create a response
    const response = {
      statusCode: 200,
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true
      },
      body: JSON.stringify(result.Attributes),
    };
    callback(null, response);
  });
};
