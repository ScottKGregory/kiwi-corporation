import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import { Home } from './components/Home';
import { Graphs } from './components/Graphs';
import AddMoron from './components/AddMoron';
import { Timeline } from './components/Timeline';

export default class App extends Component {
  static displayName = App.name;

  render () {
    return (
      <Layout>
        <Route exact path='' component={Home} />
        <Route path='/moron/add' component={AddMoron} />
        <Route path="/graphs" component={Graphs} />
      </Layout>
    );
  }
}
