import React, { Component } from "react";
import moment from "moment";

import { Address, ApiKey } from "../Constants";
import { Timeline } from "./Timeline";
import { Frustrometer } from "./componentComponents/Frustrometer";

const sumFrustrations = frustrationValues => {
  let totalDailyFrustration = 0;
  let totalRecentFrustrations = 0;
  let daysOfRecentFrustrations = [];

  for (const frustration of frustrationValues) {
    const frustrationDate = new moment(frustration.createdAt);

    const dayDifference = frustrationDate.diff(
      moment()
        .add(1, "days")
        .startOf("day"),
      "days"
    );

    // If the date is the same as today, add it to the total daily frustration
    if (dayDifference === 0) {
      totalDailyFrustration += frustration.severity + 1;
    } else if (dayDifference <= 7) {
      totalRecentFrustrations += frustration.severity + 1;

      if (!daysOfRecentFrustrations.includes(dayDifference)) {
        daysOfRecentFrustrations.push(dayDifference);
      }
    }
  }

  const result =
    totalRecentFrustrations !== 0 && daysOfRecentFrustrations.length !== 0
      ? Math.min(
          (totalDailyFrustration /
            (totalRecentFrustrations / daysOfRecentFrustrations.length)) *
            50,
          100
        )
      : 0;

  console.log(result);

  return result;
};

export class Home extends Component {
  static displayName = Home.name;

  constructor(props) {
    super(props);
    this.state = { morons: [], loading: true };
  }

  componentDidMount = () => {
    this.getMorons();
  };

  getMorons = () => {
    this.setState({ loading: true, ...this.state });
    fetch(Address + "/morons", {
      headers: {
        "Content-Type": "application/json",
        "x-api-key": ApiKey
      }
    })
      .then(response => {
        return response.json();
      })
      .then(data => {
        this.setState({ morons: data, loading: false });
      })
      .catch(e => console.log(e));
  };

  addFrustration = (moronId, severity) => {
    return fetch(Address + "/frustrations", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "x-api-key": ApiKey
      },
      body: JSON.stringify({ moronId, severity })
    })
      .then(response => {
        response.json();
        this.getMorons();
      })
      .catch(e => console.log(e));
  };

  render() {
    var morons = [];
    if (!this.state.loading && this.state.morons) {
      this.state.morons.forEach((moron, index) => {
        var filtered = moron.frustrations.filter(function(frustration) {
          return moment(frustration.createdAt).isSame(new Date(), "day");
        });
        var counts = {};
        filtered.forEach(function(x) {
          counts[x.severity] = (counts[x.severity] || 0) + 1;
        });
        morons.push(
          <div className="column is-full" key={moron.id}>
            <div className="tile is-ancestor">
              <div className="tile is-parent is-fullwidth">
                <div className="tile is-child box">
                  <div className="columns">
                    <div className="column">
                      <div className="columns">
                        <div className="column is-7">
                          <h3 className="subtitle is-3">
                            {" "}
                            {moron.firstName} "{moron.nickname}"{" "}
                            {moron.lastName}{" "}
                          </h3>
                        </div>
                        <div className="column is-5">
                          <div className="field is-grouped is-grouped-multiline is-pulled-right">
                            <div className="control">
                              <div className="tags has-addons">
                                <span className="tag is-info">Low</span>
                                <span className="tag is-light">
                                  {counts[0] || 0}
                                </span>
                              </div>
                            </div>

                            <div className="control">
                              <div className="tags has-addons">
                                <span className="tag is-warning">Medium</span>
                                <span className="tag is-light">
                                  {counts[1] || 0}
                                </span>
                              </div>
                            </div>

                            <div className="control">
                              <div className="tags has-addons">
                                <span className="tag is-danger">High</span>
                                <span className="tag is-light">
                                  {counts[2] || 0}
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <hr />
                      <div className="columns">
                        <div className="column">
                          <div className="buttons is-fullwidth">
                            <button
                              onClick={() => this.addFrustration(moron.id, 0)}
                              className="button is-medium is-fullwidth is-info"
                            >
                              Low
                            </button>
                            <button
                              onClick={() => this.addFrustration(moron.id, 1)}
                              className="button is-medium is-fullwidth is-warning"
                            >
                              Medium
                            </button>
                            <button
                              onClick={() => this.addFrustration(moron.id, 2)}
                              className="button is-medium is-fullwidth is-danger"
                            >
                              High
                            </button>
                          </div>
                        </div>
                        <div className="column is-4">
                          <Frustrometer
                            value={sumFrustrations(moron.frustrations)}
                            width={290}
                            height={150}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        );
      });
    }

    return (
      <section className="section">
        <div className="container">
          <div className="columns">
            <div className="column is-8" style={{ position: "sticky" }}>
              <div className="columns is-multiline">
                {!this.state.loading && morons}
              </div>
            </div>
            <div className="column is-4">
              <Timeline
                morons={this.state.morons}
                loading={this.state.loading}
              />
            </div>
          </div>
        </div>
      </section>
    );
  }
}
