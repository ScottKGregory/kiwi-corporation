import React, { Component } from "react";
import moment from "moment";

export class Timeline extends Component {
  static displayName = Timeline.name;

  render() {
    var frustrationEls = [];
    var allFrustrations = [];
    if (!this.props.loading && this.props.morons) {
      this.props.morons.forEach((moron, index) => {
        var filtered = moron.frustrations.filter(function(frustration) {
          return moment(frustration.createdAt).isSame(new Date(), "day");
        });
        filtered.forEach(el => (el.moronName = moron.nickname));
        allFrustrations.push(...filtered);
      });
      const severityLevels = ["Low", "Medium", "High"];
      allFrustrations.sort(
        (a, b) => moment(a.createdAt).valueOf() - moment(b.createdAt).valueOf()
      );
      allFrustrations.forEach(function(x) {
        frustrationEls.push(
          <div
            className="timeline-item is-primary"
            key={`${x.createdAt}${x.moron}`}
          >
            <div
              className={`
                      timeline-marker ${x.severity === 0 && "is-info"}
                      ${x.severity === 1 && "is-warning"}
                      ${x.severity === 2 && "is-danger"}
                    `}
            ></div>
            <div className="timeline-content">
              <p className="heading">{moment(x.createdAt).format("LT")}</p>
              <div className="tags has-addons">
                <span
                  className={`
                      tag ${x.severity === 0 && "is-info"}
                      ${x.severity === 1 && "is-warning"}
                      ${x.severity === 2 && "is-danger"}
                    `}
                >
                  {severityLevels[x.severity]}
                </span>
                <span className="tag is-light">{x.moronName}</span>
              </div>
            </div>
          </div>
        );
      });
    }

    return (
      !this.props.loading && (
        <div className="timeline is-centered">
          <header className="timeline-header">
            <span className="tag is-medium is-primary">8:00 AM</span>
          </header>
          {frustrationEls}
          <header className="timeline-header">
            <span className="tag is-medium is-primary">6:00 PM</span>
          </header>
        </div>
      )
    );
  }
}
