import React, { Component } from "react";
import { Link } from "react-router-dom";

export class Layout extends Component {
  static displayName = Layout.name;

  constructor(props) {
    super(props);
    this.state = { isExpanded: false };
  }

  render() {
    return (
      <div>
        <nav
          className="navbar is-info"
          role="navigation"
          aria-label="main navigation"
        >
          <div className="container">
            <div className="navbar-brand">
              <Link className="navbar-item" to="">
                <img src="/kiwi.png" alt="kiwi" width="28" height="28" />
              </Link>
              <a
                role="button"
                onClick={() =>
                  this.setState({
                    ...this.state,
                    isExpanded: !this.state.isExpanded
                  })
                }
                className={`navbar-burger burger ${
                  this.state.isExpanded ? "is-active" : ""
                }`}
                aria-label="menu"
                aria-expanded="false"
                data-target="navbarBasicExample"
              >
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
              </a>
            </div>

            <div
              className={`navbar-menu ${
                this.state.isExpanded ? "is-active" : ""
              }`}
            >
              <div className="navbar-start">
                <Link to="" className="navbar-item">
                  Home
                </Link>
                <Link to="/graphs" className="navbar-item">
                  Graphs
                </Link>
                <Link to="/moron/add" className="navbar-item">
                  Add moron
                </Link>
              </div>
            </div>
          </div>
        </nav>
        <section className="hero is-primary">
          <div className="hero-body">
            <div className="container">
              <h1 className="title"> Kiwi Corporation</h1>
              <h2 className="subtitle"></h2>
            </div>
          </div>
        </section>
        {this.props.children}
      </div>
    );
  }
}
