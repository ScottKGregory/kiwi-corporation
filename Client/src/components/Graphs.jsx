import React from "react";
import moment from "moment";

import { Address, ApiKey } from "../Constants";
import { LineChart } from "./componentComponents/LineChart";
import ChartistGraph from "react-chartist";
import ReactHeatMap from "react-heatmap";

const colors = [
  "#00a3de",
  "#EE204D",
  "#353535",
  "#00D1B2",
  "#1F9CEF",
  "#3373DD",
  "#22D260",
  "#FFDE57",
  "#FF3860"
];

export class Graphs extends React.Component {
  static displayName = Graphs.name;

  constructor(props) {
    super(props);
    this.state = { morons: [], loading: true };
  }

  componentDidMount = () => {
    this.getMorons();
  };

  getMorons = () => {
    this.setState({ loading: true, ...this.state });
    fetch(Address + "/morons", {
      headers: {
        "Content-Type": "application/json",
        "x-api-key": ApiKey
      }
    })
      .then(response => {
        return response.json();
      })
      .then(data => {
        this.setState({ morons: data, loading: false });
      })
      .catch(e => console.log(e));
  };

  getPiData() {
    var ret = [];
    if (!this.state.loading && this.state.morons) {
      this.state.morons.forEach((moron, index) => {
        var filtered = moron.frustrations.filter(function(frustration) {
          return moment(frustration.createdAt).isSame(new Date(), "day");
        });
        let count = 0;
        filtered.forEach(function(x) {
          count += x.severity;
        });

        ret.push({ name: `${moron.nickname}`, value: count });
      });
    }
    return { series: ret };
  }

  /**
   * Convert hours as ints to hours with :00 to them.
   * @param hour: The int hour value.
   */
  timeFormatter(hour) {
    return hour.toString() + ":00";
  }

  getLineDataByHour() {
    var ret = [];
    if (!this.state.loading && this.state.morons) {
      this.state.morons.forEach((moron, index) => {
        //Get data
        var times = [];
        for (let x = 6; x < 20; x++) {
          times.push({ x: this.timeFormatter(x), y: 0, key: x });
        }

        // Get the last 7 days worth of frustrations
        var filtered = moron.frustrations.filter(function(frustration) {
          return moment(frustration.createdAt).isAfter(
            moment()
              .subtract(7, "days")
              .startOf("day")
          );
        });

        // Fit each timestamp into the nearest hour for the graph
        for (let filter of filtered) {
          for (let i = 0; i < times.length; i++) {
            if (
              times[i].x === this.timeFormatter(moment(filter.createdAt).hour())
            ) {
              times[i].y += filter.severity + 1;
            }
          }
        }

        //Get nickname & colour and add to frustrations over time
        ret.push({
          data: times,
          name: moron.nickname,
          key: moron.nickname,
          color: colors[index]
        });
      });
    }
    return ret;
  }

  /**
   * Get the heat map data.
   */
  getHeatMapData() {
    var ret = [];
    if (!this.state.loading && this.state.morons) {
      this.state.morons.forEach((moron, index) => {
        // Get the last 7 days worth of frustrations
        var filtered = moron.frustrations.filter(function(frustration) {
          return moment(frustration.createdAt).isAfter(
            moment()
              .subtract(7, "days")
              .startOf("day")
          );
        });

        var filtered = this.state.morons;

        // Add each frustration of each moron and put their data into position
        for (let moron of filtered) {
          if (
            moron.frustrations === undefined ||
            moron.x === undefined ||
            moron.y === undefined
          ) {
            continue;
          }

          for (let frustration of moron.frustrations) {
            for (let i = 0; i < frustration.severity + 1; i++) {
              ret.push({ x: moron.x, y: moron.y, value: 1 });
            }
          }
        }
        ret.push({ x: moron.x, y: moron.y, value: 1 });
      });
    }
    console.log(ret);
    return ret;
  }

  render() {
    const frustrationOverTime = this.getLineDataByHour();

    return (
      <section className="section">
        <div className="container">
          <div className="columns">
            <div className="column">
              <h2 className="subtitle has-text-centered">
                Daily Average Frustrations
              </h2>
              {!this.state.loading && this.state.morons && (
                <LineChart frustrationData={frustrationOverTime} />
              )}
            </div>
          </div>
          <div className="columns">
            <div className="column">
              <h2 className="subtitle has-text-centered">Total Frustrations</h2>
              {!this.state.loading && this.state.morons && (
                <ChartistGraph
                  data={this.getPiData()}
                  options={{ height: "25em", width: "100%" }}
                  type={"Pie"}
                />
              )}
            </div>
          </div>
          <div className="columns">
            <div className="column" style={{ height: 300, width: 300, paddingLeft: "30%" }}>
              <h2 className="subtitle has-text-centered">Heatmap</h2>
              {
                <img
                  src={"plan.png"}
                  style={{
                    position: "absolute",
                    backgroundPosition: ""
                  }}
                />
              }
              {!this.state.loading && this.state.morons && (
                <div style={{width:625, height: 330}}>
                <ReactHeatMap max={100} data={this.getHeatMapData()} />
                  </div>
              )}
            </div>
          </div>
        </div>
      </section>
    );
  }
}
