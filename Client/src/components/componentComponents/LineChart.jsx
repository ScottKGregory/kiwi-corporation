import React from "react";
import ChartistGraph from "react-chartist";
// The line chart
export const LineChart = props => {
  let labels = [];
  let max = 0;
  props.frustrationData.forEach(f => {
    f.data.forEach(x => {
      if (!labels.includes(x.x)) labels.push(x.x);
      if (x.y > max) {
        max = x.y;
      }
    });
  });

  var series = [];
  props.frustrationData.forEach(f => {
    series.push({ data: f.data, name: "foof" });
  });

  var data = {
    labels,
    series
  };
  var options = {
    high: max,
    low: 0,
    height: "25em",
    axisX: {
      labelInterpolationFnc: (value, index) => value
    },
    plugins: []
  };

  var type = "Line";

  return <ChartistGraph data={data} options={options} type={type} />;
};
