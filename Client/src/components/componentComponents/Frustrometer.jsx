import React from "react";
import Gauge from "react-svg-gauge";

const GetColourValue = x => {
  let red = 50 - x > 0 ? x * 5.1 : 255;
  let green = x - 50 > 0 ? 255 - (x - 50) * 5.1 : 255;
  let blue = 0;

  return "rgb(" + red + ", " + green + ", " + blue + ")";
};

export const Frustrometer = props => {
  const value = Math.round(props.value);
  return (
    <Gauge
      value={value}
      width={props.width}
      height={props.height}
      color={GetColourValue(value)}
      label={null}
    />
  );
};
