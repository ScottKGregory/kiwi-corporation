import React, { Component } from "react";
import { withRouter } from "react-router-dom";

import { Address, ApiKey } from "../Constants";

class AddMoron extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: "",
      lastName: "",
      nickname: "",
      x: 0,
      y: 0
    };

    this.positionPointer = (
      <span
        style={{
          height: 5,
          width: 5,
          backgroundColor: "green",
          borderRadius: "50%",
          position: "absolute",
          top: "50",
          left: 100,
          display: "none"
        }}
      ></span>
    );
  }

  addMoron = () => {
    return fetch(Address + "/morons", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "x-api-key": ApiKey
      },
      body: JSON.stringify({
        firstName: this.state.firstName,
        lastName: this.state.lastName,
        nickname: this.state.nickname,
        x: this.state.x,
        y: this.state.y,
        frustrations: []
      })
    })
      .then(response => {
        response.json();
        this.props.history.push("");
      })
      .catch(e => console.log(e));
  };

  setUserPosition = mouse => {
    const imgWidth = document.getElementById("planImg").width;
    const imgHeight = 159;
    console.log(mouse.nativeEvent.offsetX / imgWidth);
    console.log(mouse.nativeEvent.offsetY / imgHeight);
    console.log(imgHeight);

    this.setState({
      x: (mouse.nativeEvent.offsetX / imgWidth) * 100,
      y: (mouse.nativeEvent.offsetY / imgHeight) * 100
    });

    // console.log("coords");
    // console.log((mouse.clientX / window.innerWidth) * 100);
    // console.log((mouse.clientY / window.innerHeight) * 100);
    this.positionPointer = (
      <span
        style={{
          height: 5,
          width: 5,
          backgroundColor: "green",
          borderRadius: "50%",
          position: "absolute",
          top: (mouse.clientY / window.innerHeight) * 100 + "%",
          left: (mouse.clientX / window.innerWidth) * 100 + "%"
        }}
      ></span>
    );
  };

  render() {
    return (
      <div>
        <section className="section">
          <div className="container">
            <div className="columns">
              <div className="column is-4">
                <div className="field">
                  <label className="label">First Name</label>
                  <div className="control">
                    <input
                      className="input"
                      type="text"
                      placeholder="First Name"
                      value={this.state.firstName}
                      onChange={e =>
                        this.setState({ firstName: e.target.value })
                      }
                    />
                  </div>
                </div>

                <div className="field">
                  <label className="label">Last Name</label>
                  <div className="control">
                    <input
                      className="input"
                      type="text"
                      placeholder="Last Name"
                      value={this.state.lastName}
                      onChange={e =>
                        this.setState({ lastName: e.target.value })
                      }
                    />
                  </div>
                </div>

                <div className="field">
                  <label className="label">Nickname</label>
                  <div className="control">
                    <input
                      className="input"
                      type="text"
                      placeholder="Nickname"
                      value={this.state.nickname}
                      onChange={e =>
                        this.setState({ nickname: e.target.value })
                      }
                    />
                  </div>
                </div>
                <div className="field">
                  <label className="label">Location</label>
                  <img
                    id={"planImg"}
                    src={"plan.png"}
                    alt="plan"
                    onClick={e => this.setUserPosition(e)}
                    width={300}
                  />
                </div>
                {this.positionPointer}
                <div className="field">
                  <div className="control">
                    <button
                      className="button is-primary"
                      onClick={this.addMoron}
                    >
                      Save
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default withRouter(AddMoron);
