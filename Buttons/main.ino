#include "ESPAWSClient.h"
#include <ESP8266WiFi.h>


#define TZ              -5      // (utc+) TZ in hours
#define DST_MN          60      // use 60mn for summer time in some countries

#define TZ_MN           ((TZ)*60)
#define TZ_SEC          ((TZ)*3600)
#define DST_SEC         ((DST_MN)*60)

// const char *host = "ivsxai2qus";
// const char *service = "execute-api";
// const char *key = "AKIATVJELFXOBXP72GLX";
// const char *host = "7YtiCmdSqdaDSy43h+p+YxgzQcrqOiydaRPj+2wB";
// const char *uri = "/frustrations"; //The URI will include the stage name if you are not using a custom domain name.
// const char *payload = "{\"moronId\":\"8c376760-d7e3-11e9-ab71-757ebca3ea75\",\"severity\": 1 }"";


// Local WiFi configuration
const char *ssid = "BTHub6-H8NC";
const char *password = "g64nGgChxW9c";

int led = 5;
int button = 16;
int temp = 0;

// AWS API gateway configuration
// Suppose your API gateway URI is:
//   https://ivsxai2qus.execute-api.us-east-1.amazonaws.com/
// Then:
//   host = "ivsxai2qus"
//   service = "execute-api"
//   region = "us-east-1" (the default)
//   TLD = "amazonaws.com" (the default)
const char *service = "execute-api";
const char *host = "YOUR_API_HOST";

// AWS IAM configuration
const char *key = "AKIATVJELFXOBXP72GLX";
const char *secret = "7YtiCmdSqdaDSy43h+p+YxgzQcrqOiydaRPj+2wB";

// Optional custom domain (if used, "host" field set is not used)
const char *customDomain = "kiwi-api.scottkgregory.co.uk"; // e.g. api.domain.com

void setup() {
  Serial.begin(115200);
  delay(10);

  pinMode(led, OUTPUT);
  pinMode(button, INPUT);

  // Connect to WAP
  Serial.print("Connecting to ");
  Serial.print(ssid);
  Serial.print(".");
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println(WiFi.localIP());
  Serial.println("done");
}

void post() {
  static uint32_t requests = 0;
  ESPAWSClient aws = ESPAWSClient(service, key, secret, host);
  aws.setCustomFQDN(customDomain);
  aws.setInsecure(); // Arduino 2.5.0 now requires this unless you set a fingerprint
  char *uuid = "UUID-SAMPLE";
  char uri[50], json[70];
  sprintf(uri, "/device/%s", uuid);
  sprintf(json, "{\"ip\": \"%s\", \"id\":\"%X\"}", WiFi.localIP().toString().c_str(), ESP.getChipId());
  AWSResponse resp = aws.doPost(uri, json);
  if (resp.status != 200) {
    Serial.printf("Error: %s", resp.body.c_str());
    Serial.println();
  }
  Serial.printf("Uptime: %u", millis() / 1000); Serial.println();
  Serial.printf("Requests: %u", requests++); Serial.println();
  Serial.printf("Free memory: %d", ESP.getFreeHeap()); Serial.println();
  Serial.println();
}

void loop() {
  temp = digitalRead(button);

  if (temp == HIGH)
  {
    digitalWrite(led, HIGH);
    post();
    delay(1000);
    digitalWrite(led, LOW);
  }
}