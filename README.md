# Kiwi Corporation
## Client
### JS
Building and viewing the site can be done by installing packages through `yarn` in the `/Client` directory.
Next run `yarn run start` in order to build and view the site.

### CSS
The styles use `.scss` files, with the root file being in `Client/src/scss/main.scss`. In order to build the CSS after making changes run `yarn run build-css` or `yarn run watch-css`.

#### Useful links
 - [Bulma](https://bulma.io)

 ## Server
 The server is setup using lambda and is based on an [AWS example](https://github.com/serverless/examples/tree/master/aws-node-rest-api-with-dynamodb)

 ### Deploying
 To deploy you will need to install `serverless` by running `yard global add serverless`.

 Run `serverless deploy` to use your AWS credentials and deploy the application.

 [serverless.com](https://serverless.com/framework/docs/providers/aws/guide/credentials/)